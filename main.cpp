﻿#include <iostream>

int getSharedLength(int pos1, int len1, int pos2, int len2)
{
    int e1 = pos1 + len1 - 1;
    int e2 = pos2 + len2 - 1;

    if (pos1 < pos2) {
        if (e1 < pos2) {
            return 0;
        } else if (e1 < e2) {
            return e1 - pos2 + 1;
        } else {
            return len2;
        }
    } else if (pos1 < e2) {
        if (e1 < e2) {
            return len1;
        } else {
            return e2 - pos1 + 1;
        }
    } else {
        return 0;
    }
}

int getSharedRect(int x1, int width1, int x2, int width2, int y1, int height1, int y2, int height2)
{
    return getSharedLength(x1, width1, x2, width2) * getSharedLength(y1, height1, y2, height2);
}

int calculateArea(int xa1, int ya1, int xa2, int ya2, int xb1, int yb1, int xb2, int yb2)
{
    int width1 = xa2 - xa1;
    int height1 = ya2 - ya1;
    int width2 = xb2 - xb1;
    int height2 = yb2 - yb1;

    return (width1 * height1) + (width2 * height2)
            - getSharedRect(xa1, width1, xb1, width2, ya1, height1, yb1, height2);
}

int charToInt(const char *str)
{
    char *endp = 0;
    int value = strtol(str, &endp, 10);
    if (*endp == '\0') {
        return value;
    }
    throw "Error: Invalid argument";
}

int main(int argc, char **argv)
{
    if (argc >= 9) {
        try {
            std::cout << calculateArea(charToInt(argv[1]),
                    charToInt(argv[2]),
                    charToInt(argv[3]),
                    charToInt(argv[4]),
                    charToInt(argv[5]),
                    charToInt(argv[6]),
                    charToInt(argv[7]),
                    charToInt(argv[8])) << std::endl;
        } catch(const char *e) {
            std::cout << e << std::endl;
        }
    } else {
        std::cout << "Error: Few arguments" << std::endl;
    }
    return 0;
}

